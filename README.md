# Game Time V1.0

Small implementation of ISO calenda, based on Chrono crate, and minutes::hour timer.

## What is this?

Basically it consist on a sigle class (**Timer**) which can be created with Timer::new( ... ).
This function takes as arguments the starting year (have to be valid data) and sets a
date type from chrono crate on day 1 of month 1 of given year and starts minute and hour timer in
00:00.

We can also create it with Timer::new_from_ymd(...) and set starting year, month and day, but if
given data doesn't represent a valid date, chrono will panic.

Later we will be using step() function to advance one minute step which will internally update
internal chrono date, minutes and hours. It also contains an step counter (_u64_) that can be used
to eficiently compare is some condition is met in a given X time.

Also, all the info related to the current time point can be retrieved with the functions
get_xxxxx().

Finally, and in order to support different language names for weekday and months, we can use the
function **set_language(..)** to set returning string to our desired language. This function takes
as parameter a public enum Language that is also defined in timer.rs. for the moment, options are
only Spanish or English, but language adition is straight forward and can be done within a minute ;)

## Usage Example

On Cargo.toml file:

```
[dependencies]
game_time = "1.0"

```

On your .rs source file:

```rust

use game_time::{Timer, Language};

fn foo() {
    // Notice that new_from_ymd will panic if given data are not a valid date
    let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
    t.set_language(Language::English);
    for _ in 0..5000000 {
        t.step();
    }
    assert_eq!(t.get_t(), 5000000);
    assert_eq!(t.get_min(), 20);
    assert_eq!(t.get_hour(), 5);
    assert_eq!(t.get_day_year(), 220);
    assert_eq!(t.get_day(), 8);
    assert_eq!(t.get_month(), 8);
    assert_eq!(t.get_month_string(), String::from("August"));
    assert_eq!(t.get_year(), 1509);
    assert_eq!(t.get_days_from_t0(), 3472);
    assert_eq!(t.get_weekday(), 7);
    assert_eq!(t.get_weekday_string(), String::from("Sunday"));
}


```

# License

This software us licensed under License GPL-3.0-only.

Copyright Joseba Martinez 2020  (josebam@protonmail.com).

