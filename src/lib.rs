/*
 * License GPL-3.0-only
 *
 * Copyright Joseba Martinez 2020 (josebam@protonmail.com)
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, with version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

pub mod timer;

#[cfg(test)]
mod tests {
    use super::timer::{Timer, Language};
    #[test]
    fn t_0() {
        let t: Timer = Timer::new_from_ymd(1500, 2, 4);
        assert_eq!(t.get_t(), 0);
        assert_eq!(t.get_min(), 0);
        assert_eq!(t.get_hour(), 0);
        assert_eq!(t.get_day_year(), 35);
        assert_eq!(t.get_day(), 4);
        assert_eq!(t.get_month(), 2);
        assert_eq!(t.get_month_string(), String::from("Febrero"));
        assert_eq!(t.get_year(), 1500);
        assert_eq!(t.get_days_from_t0(), 0);
        assert_eq!(t.get_weekday(), 7);
        assert_eq!(t.get_weekday_string(), String::from("Domingo"));
    }

    #[test]
    fn t_1() {
        let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
        t.step();
        assert_eq!(t.get_t(), 1);
        assert_eq!(t.get_min(), 1);
        assert_eq!(t.get_hour(), 0);
        assert_eq!(t.get_day_year(), 35);
        assert_eq!(t.get_day(), 4);
        assert_eq!(t.get_month(), 2);
        assert_eq!(t.get_month_string(), String::from("Febrero"));
        assert_eq!(t.get_year(), 1500);
        assert_eq!(t.get_days_from_t0(), 0);
        assert_eq!(t.get_weekday(), 7);
        assert_eq!(t.get_weekday_string(), String::from("Domingo"));
    }

    #[test]
    fn t_100() {
        let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
        for _ in 0..100 {
            t.step();
        }
        assert_eq!(t.get_t(), 100);
        assert_eq!(t.get_min(), 40);
        assert_eq!(t.get_hour(), 1);
        assert_eq!(t.get_day_year(), 35);
        assert_eq!(t.get_day(), 4);
        assert_eq!(t.get_month(), 2);
        assert_eq!(t.get_month_string(), String::from("Febrero"));
        assert_eq!(t.get_year(), 1500);
        assert_eq!(t.get_days_from_t0(), 0);
        assert_eq!(t.get_weekday(), 7);
        assert_eq!(t.get_weekday_string(), String::from("Domingo"));
    }

    #[test]
    fn t_10000() {
        let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
        for _ in 0..10000 {
            t.step();
        }
        assert_eq!(t.get_t(), 10000);
        assert_eq!(t.get_min(), 40);
        assert_eq!(t.get_hour(), 22);
        assert_eq!(t.get_day_year(), 41);
        assert_eq!(t.get_day(), 10);
        assert_eq!(t.get_month(), 2);
        assert_eq!(t.get_month_string(), String::from("Febrero"));
        assert_eq!(t.get_year(), 1500);
        assert_eq!(t.get_days_from_t0(), 6);
        assert_eq!(t.get_weekday(), 6);
        assert_eq!(t.get_weekday_string(), String::from("Sábado"));
    }

    #[test]
    fn t_5000000() {
        let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
        for _ in 0..5000000 {
            t.step();
        }
        assert_eq!(t.get_t(), 5000000);
        assert_eq!(t.get_min(), 20);
        assert_eq!(t.get_hour(), 5);
        assert_eq!(t.get_day_year(), 220);
        assert_eq!(t.get_day(), 8);
        assert_eq!(t.get_month(), 8);
        assert_eq!(t.get_month_string(), String::from("Agosto"));
        assert_eq!(t.get_year(), 1509);
        assert_eq!(t.get_days_from_t0(), 3472);
        assert_eq!(t.get_weekday(), 7);
        assert_eq!(t.get_weekday_string(), String::from("Domingo"));
    }

    #[test]
    fn t_5000000_eng() {
        let mut t: Timer = Timer::new_from_ymd(1500, 2, 4);
        t.set_language(Language::English);
        for _ in 0..5000000 {
            t.step();
        }
        assert_eq!(t.get_t(), 5000000);
        assert_eq!(t.get_min(), 20);
        assert_eq!(t.get_hour(), 5);
        assert_eq!(t.get_day_year(), 220);
        assert_eq!(t.get_day(), 8);
        assert_eq!(t.get_month(), 8);
        assert_eq!(t.get_month_string(), String::from("August"));
        assert_eq!(t.get_year(), 1509);
        assert_eq!(t.get_days_from_t0(), 3472);
        assert_eq!(t.get_weekday(), 7);
        assert_eq!(t.get_weekday_string(), String::from("Sunday"));
    }
}
