/*
 * License GPL-3.0-only
 *
 * Copyright Joseba Martinez 2020 (josebam@protonmail.com)
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, with version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

use chrono::{Datelike, Duration, IsoWeek, NaiveDate, Weekday};

pub enum Language {
    Spanish,
    English,
}

pub struct Timer {
    date: NaiveDate,
    m: u8,
    h: u8,
    t: u64,
    l: Language,
}

impl Timer {
    pub fn new(year: i32) -> Timer {
        Timer {
            date: NaiveDate::from_ymd(year, 1, 1),
            m: 0,
            h: 0,
            t: 0,
            l: Language::Spanish,
        }
    }

    pub fn new_from_ymd(year: i32, month: u32, day: u32) -> Timer {
        Timer {
            date: NaiveDate::from_ymd(year, month, day),
            m: 0,
            h: 0,
            t: 0,
            l: Language::Spanish,
        }
    }

    pub fn set_language(&mut self, language: Language) {
        self.l = language;
    }

    pub fn step(&mut self) {
        self.t += 1;
        if self.m < 59 {
            self.m += 1;
        } else {
            self.m = 0;
            if self.h < 23 {
                self.h += 1;
            } else {
                self.h = 0;
                self.date += Duration::days(1);
            }
        }
    }

    pub fn get_t(&self) -> u64 {
        self.t
    }

    pub fn get_min(&self) -> u8 {
        self.m
    }

    pub fn get_hour(&self) -> u8 {
        self.h
    }

    pub fn get_day_year(&self) -> u32 {
        self.date.ordinal()
    }

    pub fn get_day(&self) -> u32 {
        self.date.day()
    }

    pub fn get_month(&self) -> u32 {
        self.date.month()
    }

    pub fn get_month_string(&self) -> String {
        match self.l {
            Language::Spanish => String::from(month_str::ESP[self.date.month0() as usize]),
            Language::English => String::from(month_str::ENG[self.date.month0() as usize]),
        }
    }

    pub fn get_year(&self) -> i32 {
        self.date.year()
    }

    pub fn get_days_from_t0(&self) -> u64 {
        self.t / 1440
    }

    pub fn get_weekday(&self) -> u8 {
        1 + self.date.weekday() as u8
    }

    pub fn get_week_year(&self) -> u32 {
        self.date.iso_week().week()
    }

    pub fn get_weekday_string(&self) -> String {
        match self.l {
            Language::Spanish => String::from(week_day_str::ESP[self.date.weekday() as usize]),
            Language::English => String::from(week_day_str::ENG[self.date.weekday() as usize]),
        }
    }
}

// Weekday names
mod week_day_str {
    pub const ESP: [&'static str; 7] = [
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado",
        "Domingo",
    ];

    pub const ENG: [&'static str; 7] = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ];
}

// Month names
mod month_str {
    pub const ESP: [&'static str; 12] = [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];

    pub const ENG: [&'static str; 12] = [
        "Jaunary",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
}
